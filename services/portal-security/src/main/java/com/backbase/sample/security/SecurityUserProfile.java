package com.backbase.sample.security;

/**
 * @author Eugene Rozov
 * mailto: eugener@backbase.com
 */
public class SecurityUserProfile {

    private String userLogin;

    private String language;

    private String[] screenVisibility;

    // todo: other fields like mobileDevices and preferences


    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String[] getScreenVisibility() {
        return screenVisibility;
    }

    public void setScreenVisibility(String[] screenVisibility) {
        this.screenVisibility = screenVisibility;
    }
}
