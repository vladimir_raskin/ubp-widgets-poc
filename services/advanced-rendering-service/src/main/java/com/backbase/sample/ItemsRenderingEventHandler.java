package com.backbase.sample;

import com.backbase.portal.foundation.domain.conceptual.Item;
import com.backbase.portal.foundation.domain.conceptual.PropertyDefinition;
import com.backbase.portal.foundation.domain.conceptual.StringArrayPropertyValue;
import com.backbase.portal.foundation.domain.model.BaseContainer;
import com.backbase.portal.foundation.domain.model.User;
import com.backbase.portal.foundation.domain.presentation.event.BeforeServerSideRenderingEvent;
import com.backbase.portal.foundation.extensionapi.event.domain.Event;
import com.backbase.portal.foundation.extensionapi.event.handler.AbstractSynchronousEventHandler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author Eugene Rozov
 * mailto: eugener@backbase.com
 */
@Component
public class ItemsRenderingEventHandler extends AbstractSynchronousEventHandler {

    private static final String VISIBILITY_FIELD = "screenVisibility";

    public ItemsRenderingEventHandler() {
        super(BeforeServerSideRenderingEvent.class);
    }

    @Override
    public void handleEvent(Event event) {
        // get list of whitelisted GUIDs and hide all items, which has defined GUID, but field value is not in the white list.
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user != null && user.getPropertyDefinitions().containsKey(VISIBILITY_FIELD)) {
            Item rootItem = ((BeforeServerSideRenderingEvent) event).getRootItem();
            filterItems(rootItem, getAllowedIds(user));
        }
    }

    private void filterItems(Item item, Collection<String> allowedIds) {
        if (item instanceof BaseContainer) {
            List<Item> children = ((BaseContainer) item).getChildren();
            List<Item> toRemove = new ArrayList<>();
            for (Item child : children) {
                PropertyDefinition guid = child.getProperties().get("GUID");
                if (guid == null || allowedIds.contains(guid.getValue().toString())) {
                    filterItems(child, allowedIds);
                } else {
                    toRemove.add(child);
                }
            }
            children.removeAll(toRemove);
        }
    }

    private static Collection<String> getAllowedIds(User user) {
        StringArrayPropertyValue screenVisibility = (StringArrayPropertyValue) user.getPropertyDefinitions().get(VISIBILITY_FIELD).getValue();
        return new HashSet<>(Arrays.asList(screenVisibility.getValue()));
    }

}
