package com.backbase.portal.foundation.presentation.renderer.gmodel.renderer.navigation;

import com.backbase.portal.commons.api.logging.LoggerFactory;
import com.backbase.portal.commons.api.logging.StrictLogger;
import com.backbase.portal.foundation.business.caching.FoundationCacheProvider;
import com.backbase.portal.foundation.business.service.LinkBusinessService;
import com.backbase.portal.foundation.business.utils.context.ThreadLocalRequestContext;
import com.backbase.portal.foundation.commons.exceptions.ItemNotFoundException;
import com.backbase.portal.foundation.domain.conceptual.Item;
import com.backbase.portal.foundation.domain.conceptual.PropertyDefinition;
import com.backbase.portal.foundation.domain.conceptual.StringArrayPropertyValue;
import com.backbase.portal.foundation.domain.model.BaseContainer;
import com.backbase.portal.foundation.domain.model.Link;
import com.backbase.portal.foundation.domain.model.User;
import com.backbase.portal.foundation.integration.resource.ResourceReader;
import com.backbase.portal.foundation.presentation.logging.LogEvents;
import com.backbase.portal.foundation.presentation.renderer.gmodel.mustache.AdvancedNavigationLink;
import com.backbase.portal.foundation.presentation.urllevelcache.filter.RequestUrlStoringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * The renderer is located in this package because it depends on {@code NavigationDisplayParams}, which has package visibility.
 *
 * @author Eugene Rozov
 * mailto: eugener@backbase.com
 */
@Service
public class SecuredNavRenderer extends AdvancedNavRenderer {

    private static final StrictLogger LOGGER = LoggerFactory.PS.logger(SecuredNavRenderer.class);

    private static final String VISIBILITY_FIELD = "screenVisibility";

    private static final Set<String> KEYS;

    private final LinkBusinessService linkBusinessService;

    static {
        Set<String> tmpKeys = new HashSet<>(1);
        tmpKeys.add("securedNavRenderer");
        KEYS = Collections.unmodifiableSet(tmpKeys);
    }

    @Autowired
    public SecuredNavRenderer(@Qualifier("foundationCacheProvider") FoundationCacheProvider foundationCacheProvider, @Qualifier("rootResourceReader") ResourceReader pResourceReader, LinkBusinessService linkBusinessService, NavStartResolverRegistry navResolver, RequestUrlStoringHelper requestUrlStoringHelper) {
        super(foundationCacheProvider, pResourceReader, linkBusinessService, navResolver, requestUrlStoringHelper);
        this.linkBusinessService = linkBusinessService;
    }

    @Override
    public Set<String> getKeys() {
        return KEYS;
    }

    @Override
    protected List<AdvancedNavigationLink> getLinksToShow(String rootUuid, String contextPath, NavigationDisplayParams params) {
        ArrayList<Link> rootList = new ArrayList<>();

        try {
            if (rootUuid != null) {
                int renderDepth = this.calcRenderDepth(params);
                Link root = this.linkBusinessService.getItemByUuid(rootUuid, true, renderDepth);
                filterLinks(root);
                rootList.add(root);
            }
        } catch (ItemNotFoundException var7) {
            LOGGER.warn(LogEvents.WARN.NAV_TYPE_NOT_FOUND, rootUuid);
        }

        return this.stripTopLevels(rootList, contextPath, params);
    }

    private void filterLinks(Link rootItem) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user != null && user.getPropertyDefinitions().containsKey(VISIBILITY_FIELD)) {
            filterItems(rootItem, getAllowedIds(user));
        }
    }

    private void filterItems(Item item, Collection<String> allowedIds) {
        if (item instanceof BaseContainer) {
            List<Item> children = ((BaseContainer) item).getChildren();
            List<Item> toRemove = new ArrayList<>();
            for (Item child : children) {
                PropertyDefinition guid = child.getProperties().get("GUID");
                if (guid == null || allowedIds.contains(guid.getValue().toString())) {
                    filterItems(child, allowedIds);
                } else {
                    toRemove.add(child);
                }
            }
            children.removeAll(toRemove);
        }
    }

    private static Collection<String> getAllowedIds(User user) {
        StringArrayPropertyValue screenVisibility = (StringArrayPropertyValue) user.getPropertyDefinitions().get(VISIBILITY_FIELD).getValue();
        return new HashSet<>(Arrays.asList(screenVisibility.getValue()));
    }

    /* copy from AdvancedNavRenderer */

    private int calcRenderDepth(NavigationDisplayParams params) {
        int depth = params.getInt("depth", 1);
        int navLevel = this.getNavLevel(params);
        int renderDepth;
        if (depth < 0) {
            renderDepth = -1;
        } else {
            renderDepth = depth + navLevel;
        }

        if (renderDepth == 0) {
            renderDepth = 1;
        }

        return renderDepth;
    }

    private int getNavLevel(NavigationDisplayParams params) {
        boolean showParent = params.getBoolean("showParent");
        Integer navLevel = params.getDeprecatedInt("navLevel", "showParent", 0);
        if (navLevel == null) {
            if (showParent) {
                navLevel = 0;
            } else {
                navLevel = 1;
            }
        }

        return navLevel;
    }

    private List<AdvancedNavigationLink> stripTopLevels(List<Link> itemsToBeRendered, String contextPath, NavigationDisplayParams params) {
        List<AdvancedNavigationLink> resultModel = new ArrayList();
        boolean useGeneratedUrl = params.getBoolean("useGeneratedUrl");
        int navLevel = this.getNavLevel(params);
        if (itemsToBeRendered != null && !itemsToBeRendered.isEmpty()) {
            List<Link> linksToRender = itemsToBeRendered;

            for(int i = 0; i < navLevel; ++i) {
                linksToRender = this.getChildren(linksToRender);
            }

            Iterator i$ = linksToRender.iterator();

            while(i$.hasNext()) {
                Object obj = i$.next();
                resultModel.add(new AdvancedNavigationLink((Link)obj, contextPath, useGeneratedUrl, this.getCurrentLinkUuid()));
            }
        }

        return resultModel;
    }

    private List<Link> getChildren(List<Link> links) {
        List<Link> result = new ArrayList();
        Iterator i$ = links.iterator();

        while(i$.hasNext()) {
            Link link = (Link)i$.next();
            Iterator i1$ = link.getChildren().iterator();

            while(i1$.hasNext()) {
                Item childLink = (Item)i1$.next();
                result.add((Link)childLink);
            }
        }

        return result;
    }

    private String getCurrentLinkUuid() {
        Link currentLink = ThreadLocalRequestContext.getContext().getCurrentLink();
        String currentLinkUuid;
        if (currentLink == null) {
            currentLinkUuid = null;
        } else {
            currentLinkUuid = currentLink.getUuid();
        }

        return currentLinkUuid;
    }
}
